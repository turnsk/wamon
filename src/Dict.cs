/*	http://code.google.com/p/wamon/
 *	Windows Activity Monitor - Monitors user's activity on the computer, 
 *	tracks active windows and processes, allows table and chart visualization.
 *	Copyright (C) 2009  Archae s.r.o. (http://archaedev.com/)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Collections.Generic;
using System.Globalization;
using System.Xml;

namespace Archae.Wamon
{
	class Dict
	{
		private static Dict singleton = null;
		private string language = "en-US";
		private XmlDocument xmlDoc = new XmlDocument();
		private Dictionary<string, string> dict = new Dictionary<string,string>();
		internal static Dict Singleton
		{
			get
			{
				if (singleton == null)
					singleton = new Dict();
				return singleton;
			}
		}
		private Dict()
		{
			xmlDoc = null;
		}
		internal Dictionary<string, string> SupportedLanguages
		{
			get
			{
				Dictionary<string, string> languages = new Dictionary<string, string>();
				if (xmlDoc != null)
				{
					XmlNodeList nodeList = xmlDoc.SelectNodes("wam/dict");
					foreach (XmlNode node in nodeList)
					{
						if (node.Attributes["id"] != null && node.Attributes["name"] != null)
							languages[node.Attributes["id"].Value] = node.Attributes["name"].Value;
					}
				}
				return languages;
			}
		}
		internal CultureInfo CultureInfo
		{
			get { return new CultureInfo(language); }
		}
		internal void Load(string xml, string language)
		{
			
			this.language = language;
			xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xml);
			XmlNodeList nodeList = xmlDoc.SelectNodes("wam/dict[@id='" + language + "']/entry");
			if (nodeList.Count == 0)
			{
				Load(xml, "en-US");
				return;
			}
			foreach (XmlNode node in nodeList)
			{
				if (node.Attributes["id"] != null)
					dict[node.Attributes["id"].Value] = node.InnerText;
			}
		}
		internal string Language
		{
			get { return language; }
			set
			{
				XmlNodeList nodeList = xmlDoc.SelectNodes("wam/dict[@id='" + value + "']/entry");
				if (nodeList.Count == 0)
				{
					return;
				}
				dict.Clear();
				foreach (XmlNode node in nodeList)
				{
					if (node.Attributes["id"] != null)
						dict[node.Attributes["id"].Value] = node.InnerText;
				}
				this.language = value;
			}
		}
		internal string this[string id]
		{
			get { return (dict.ContainsKey(id) ? dict[id] : id); }
		}
	}
}
