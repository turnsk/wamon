[Setup]
AppName=Windows Activity Monitor
AppVerName=Windows Activity Monitor 1.3
AppPublisher=Archae s.r.o.
AppPublisherURL=http://archaedev.com/
AppSupportURL=http://code.google.com/p/wamon/w/list
AppUpdatesURL=http://code.google.com/p/wamon/downloads/list
AppCopyright=Copyright (c) 2012, Archae s.r.o.
DefaultDirName={pf}\Wamon
LicenseFile=license.rtf
MinVersion=5.01,5.01
WizardImageFile=SetupModern16.bmp
WizardSmallImageFile=SetupModernSmall16.bmp
OutputDir=.
OutputBaseFilename=wamon13
RestartIfNeededByRun=no
DisableDirPage=yes
DisableProgramGroupPage=yes
DisableReadyPage=yes
VersionInfoTextVersion={#GetFileVersion('..\bin\wamon.exe')}
VersionInfoVersion={#GetFileVersion('..\bin\wamon.exe')}
ArchitecturesInstallIn64BitMode=x64 ia64

[Tasks]
Name: shortcut; Description: Shortcut in Start Menu; Flags: unchecked

[Files]
Source: "..\bin\wamon.exe"; DestDir: {app}; Flags: ignoreversion
Source: "..\bin\System.Data.SQLite.dll"; DestDir: {app}; Check: not(IsWin64)
Source: "..\bin\System.Data.SQLite-x64.dll"; DestDir: {app}; DestName: System.Data.SQLite.dll; Check: IsWin64
Source: "..\bin\Interop.NETCONLib.dll"; DestDir: {app}
Source: "..\bin\Interop.NetFwTypeLib.dll"; DestDir: {app}
Source: "..\bin\msvcp100.dll"; DestDir: {sys}; Flags: onlyifdoesntexist; Check: not(IsWin64)
Source: "..\bin\msvcr100.dll"; DestDir: {sys}; Flags: onlyifdoesntexist; Check: not(IsWin64)
Source: "..\bin\wam.dat"; DestDir: {commonappdata}\wam; Permissions: everyone-full; Flags: onlyifdoesntexist uninsneveruninstall
Source: "license.rtf"; DestDir: {app}

[Icons]
Name: "{commonprograms}\Windows Activity Monitor"; Filename: "{app}\wamon.exe"; Parameters: "/stats"; Tasks: shortcut

[Registry]
Root: HKLM; Subkey: Software\Microsoft\Windows\CurrentVersion\Run; ValueType: string; ValueName: "Windows Activity Monitor"; ValueData: "{app}\wamon.exe /daemon"; Flags: uninsdeletevalue

[Run]
Filename: "{app}\wamon.exe"; Parameters: "/install"; StatusMsg: "Installing Wamon..."; Flags: runhidden
Filename: "{app}\wamon.exe"; Parameters: "/daemon"; StatusMsg: "Starting Wamon..."; Flags: runhidden nowait

[UninstallRun]
Filename: "{app}\wamon.exe"; Parameters: "/uninstall"; Flags: runhidden

[UninstallDelete]
Type: filesandordirs; Name: "{pf}\Wamon"

[Code]
function InitializeSetup(): Boolean;
begin
  CreateMutex('WamonReinstall');
  Sleep(1000);
  Result := True;
end;
function InitializeUninstall(): Boolean;
begin
  CreateMutex('WamonReinstall');
  Sleep(1000);
  Result := True;
end;
