# Windows Activity Monitor #

### You can download the latest version [here](https://bitbucket.org/turnsk/wamon/downloads/wamon13.exe). ###

The Windows Activity Monitor monitors user's activity on the computer, tracks active windows and processes, allows table and chart visualization. If you have Wamon installed with default settings you can [click here](http://127.0.0.1:57824/stats) to see the statistics.

If you find this application useful consider [donation](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=5077228) to support further development.

## Where can I use this? ##

Did you ever wonder what did you spend the whole day doing? Are you interested in what others are doing on your computer? Do you suspect your employees of doing anything else but working? Windows Activity Monitor can help you answer these questions.

## How does it work? ##

Windows Activity Monitor periodically records foreground window and its process executable to give out various statistics and charts. You can group windows and/or processes using regular expressions and see simplified output like "Work", "School", "Fun", etc.

Additionally, thanks to its browser interface you can set Wamon to watch the computer activity or statistics from a different computer.

## Things to consider before using Wamon ##

You should always let people know that you are monitoring their activity on the computer. Although we have tried to make it not easy to disable or remove, an experienced user may be able to disable the application.

## Who's behind this all? ##

Windows Activity Monitor is developed and managed by [Turn](https://turn.sk/).